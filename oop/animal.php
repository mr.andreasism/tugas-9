<?php

class Animal 
{
    public $legs = 4;
    public $blood = "no";
    public $name;
    
    public function __construct($nama)
    {
    $this->name = $nama;
    }
}

class Frog extends Animal

{

public function jump()
{
    return "Jump : Hop Hop <br> <br>";
}
}

class Ape extends Animal

{
public $apeLegs = 2;

public function yell()
{
    return "aiueo <br> <br>";
}
}
?>
